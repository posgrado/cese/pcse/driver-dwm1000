[[_TOC_]]

# Resources
- https://www.decawave.com/software/
- https://github.com/Decawave

## PANS Library
- https://www.decawave.com/wp-content/uploads/2019/01/DWM1001-Firmware-User-Guide-2.1.pdf
- https://www.decawave.com/wp-content/uploads/2019/01/DWM1001-API-Guide-2.2.pdf

## TDoA
- https://www.decawave.com/wp-content/uploads/2019/11/DWM1001_TDoA_TAG_1_3.zip

## Development Kit (Software + Firmware)
- https://www.decawave.com/wp-content/uploads/2019/03/DWM1001_DWM1001-DEV_MDEK1001_Sources_and_Docs_v9.zip

## Apache Mynewt
- https://github.com/Decawave/mynewt-dw1000-core
- https://github.com/Decawave/mynewt-dw1000-apps

## Simple C examples (FreeRTOS optional)
- https://github.com/Decawave/dwm1001-examples